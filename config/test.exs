use Mix.Config

db_url = System.get_env("DB_URL") || "postgres://postgres:postgres@localhost/mana"

# compile time config
config :mana_elixir, ManaElixir.Db.Repo,
  url: db_url,
  pool: Ecto.Adapters.SQL.Sandbox

config :mana_elixir,
  account_manager: ManaElixir.AccountManagerMock,
  game_server: ManaElixir.GameServerMock,
  game_server_manager: ManaElixir.GameServerManagerMock,
  token_based_storer: ManaElixir.TokenbasedDataStorerMock

# defaults
config :mana_elixir,
  client_data_uri: "dummy",
  game_server_address: "dummy",
  game_server_port: 9803,
  chat_server_address: "dummy",
  chat_server_port: 9804,
  db_url: db_url

# speed up argon2 (for tests only!)
config :argon2_elixir, t_cost: 2, m_cost: 8
