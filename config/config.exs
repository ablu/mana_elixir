use Mix.Config

config :logger, level: :debug

# compile time config
config :mana_elixir,
  ecto_repos: [ManaElixir.Db.Repo],
  account_manager: ManaElixir.DbBackedAccountManager,
  game_server: ManaElixir.GameServerImpl,
  game_server_manager: ManaElixir.GameServerManagerImpl,
  token_based_storer: ManaElixir.InMemoryTokenbasedDataStorer

# environment overridable defaults
config :mana_elixir,
  max_num_of_chars: 3,
  chat_server_address: "localhost",
  chat_server_port: 9603,
  game_server_address: "localhost",
  game_server_port: 9604

import_config "#{Mix.env()}.exs"
