defmodule ManaElixir.MixProject do
  use Mix.Project

  def project do
    [
      app: :mana_elixir,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {ManaElixir, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:enet,
       git: "https://github.com/flambard/enet.git",
       tag: "38cbe2de3553ee78ac2fc4a5dff538239fcd549c"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, "~> 0.14.1"},
      {:comeonin, "~> 4.1"},
      {:argon2_elixir, "~> 1.3"},
      {:mox, "~> 0.4", only: :test},
      {:dialyxir, "~> 1.0.0-rc.4", only: [:dev], runtime: false}
    ]
  end

  defp aliases do
    [
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
