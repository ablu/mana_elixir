defmodule ManaElixir.InMemoryTokenbasedDataStorerTest do
  import ManaElixir.InMemoryTokenbasedDataStorer
  use ExUnit.Case
  doctest ManaElixir.InMemoryTokenbasedDataStorer

  setup do
    {:ok, server: start_supervised!(ManaElixir.InMemoryTokenbasedDataStorer)}
  end

  test "storage and retrieval of token", state do
    token = register_data("data", 100, state.server)
    assert get_data(token, state.server) == "data"
  end

  test "cleanup works", state do
    token = register_data("data which should time out", 0, state.server)
    Process.sleep(100)
    assert get_data(token, state.server) == nil
  end
end
