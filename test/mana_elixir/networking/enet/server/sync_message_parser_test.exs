defmodule ManaElixir.Networking.Enet.Server.SyncMessageParserTest do
  import ManaElixir.Networking.Enet.Server.SyncMessageParser
  use ExUnit.Case
  doctest ManaElixir.Networking.Enet.Server.SyncMessageParser

  test "parses multiple sync messages" do
    assert parse(
             <<4, 0, 0, 0, 4, 1, 2, 0, 0, 0, 4, 0, 0, 0, 14, 3, 55, 46, 53, 3, 55, 46, 53, 2, 0,
               0, 0, 4, 0, 0, 0, 16, 1, 51, 1, 51>>
           ) == [
             {:sync_online_status, 4, true},
             {:sync_character_attribute, 4, 14, 7.5, 7.5},
             {:sync_character_attribute, 4, 16, 3, 3}
           ]
  end
end
