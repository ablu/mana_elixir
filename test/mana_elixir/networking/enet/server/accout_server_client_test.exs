defmodule ManaElixir.Networking.Enet.Server.AccountServerClientTest do
  use ExUnit.Case
  doctest ManaElixir.Networking.Enet.Server.AccountServerClient
  import ManaElixir.Networking.Enet.Protocol
  import Mox
  alias ManaElixir.Db.Account
  alias ManaElixir.Db.Character
  alias ManaElixir.Networking.Enet.Server.AccountServerClient

  defp fake_send(msg, state) do
    {:noreply, new_state} = AccountServerClient.handle_info({:enet, 0, {:reliable, msg}}, state)
    new_state
  end

  setup :verify_on_exit!

  setup do
    pid = self()

    initial_state = %AccountServerClient.State{
      channel: fn msg -> send(pid, msg) end,
      client_data_uri: "",
      max_num_of_chars: 1,
      game_server_address: "",
      game_server_port: 0,
      chat_server_address: "",
      chat_server_port: 0
    }

    {:ok, initial_state: initial_state}
  end

  test "login works", state do
    ManaElixir.AccountManagerMock
    |> expect(:check_login, fn _, _, _ -> {:ok, %Account{characters: []}} end)

    state = fake_send(pamsg_login_rndtrgr({""}), state.initial_state)

    receive do
      msg -> assert match?({:apmsg_login_rndtrgr_response, _}, parse(msg))
    after
      0 -> flunk("expected salt response")
    end

    fake_send(pamsg_login({1, "test", "test"}), state)
    expected_message = apmsg_login_response({errmsg_ok(), "", "", 1, <<>>})
    assert_received ^expected_message
  end

  test "creating char works", state do
    ManaElixir.AccountManagerMock
    |> expect(:create_char, fn _acc, char -> {:ok, Map.merge(%Character{}, char)} end)

    s = %AccountServerClient.State{state.initial_state | account: %Account{characters: []}}
    s = fake_send(pamsg_char_create({"Test", 0, 0, 0, 0, <<>>}), s)

    msg =
      apmsg_char_create_response({
        errmsg_ok(),
        char_data({0, "Test", 0, 0, 0, 0, 0, 0, <<>>, 0, <<>>})
      })

    assert_received ^msg

    assert Enum.at(s.account.characters, 0) == %Character{
             name: "Test",
             gender: 0,
             hair_color: 0,
             hair_style: 0
           }
  end

  test "choosing char and thus forwarding to game server works", state do
    ManaElixir.TokenbasedDataStorerMock
    |> expect(:register_data, fn _ -> <<0::256>> end)

    state = %AccountServerClient.State{
      state.initial_state
      | account: %Account{characters: [%Character{}]}
    }

    fake_send(pamsg_char_select({0}), state)

    receive do
      msg -> assert match?({:apmsg_char_select_response, _}, parse(msg))
    after
      0 -> flunk("expected char select response")
    end
  end
end
