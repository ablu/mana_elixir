defmodule ManaElixir.Networking.Enet.DebugTest do
  use ExUnit.Case
  use Bitwise
  doctest ManaElixir.Networking.Enet.Debug
  import ManaElixir.Networking.Enet.Debug
  import ManaElixir.Networking.Enet.Protocol

  test "strips debug from command" do
    assert strip_debug_info(<<dbg_flag() ||| 0x0001::16>>) == <<0x0001::16>>
  end

  test "leave non debug is left intact" do
    assert strip_debug_info(<<0x0001::16>>) == <<0x0001::16>>
  end

  test "strips value types from message" do
    assert strip_debug_info(
             <<dbg_flag() ||| 0x0001::16, dbg_val_type_int8()::8, 0x02::8,
               dbg_val_type_int16()::8, 0x0003::16, dbg_val_type_int32()::8, 0x00000004::32,
               dbg_val_type_string()::8, dbg_val_type_int16()::8, -1::signed-size(16),
               dbg_val_type_int16()::8, 4::16, "test">>
           ) == <<0x0001::16, 0x02::8, 0x0003::16, 0x00000004::32, 4::16, "test">>
  end

  test "strip debug info from login rndtrgr message" do
    assert strip_debug_info(<<128, 21, 3, 1, 255, 255, 1, 0, 4, 116, 101, 115, 116>>) ==
             <<21::16, 4::16, "test">>
  end

  test "strip debug info from token" do
    token =
      <<86, 239, 191, 189, 38, 116, 239, 191, 189, 73, 239, 191, 189, 239, 191, 189, 203, 134,
        239, 191, 189, 239, 191, 189, 212, 164, 239, 191, 189, 239, 191, 189>>

    assert strip_debug_info(<<128, 83, 3, 1, 0, 32, token::binary>>) ==
             <<0x0053::16, token::binary>>
  end
end
