defmodule ManaElixir.GameServerManagerTest do
  import ManaElixir.GameServerManagerImpl
  use ExUnit.Case
  doctest ManaElixir.GameServerManagerImpl

  test "the first server actives all maps" do
    pid = start_supervised!(ManaElixir.GameServerManagerImpl)
    assert register_server("name", self(), pid) == [1]
    assert get_server_for_map(1, pid) == self()
  end
end
