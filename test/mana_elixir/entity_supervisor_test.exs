defmodule ManaElixir.EntitySupervisorTest do
  import ManaElixir.EntitySupervisor
  use ExUnit.Case
  doctest ManaElixir.EntitySupervisor

  test "a character can be inserted as entity" do
    pid = start_supervised!(ManaElixir.EntitySupervisor)

    ManaElixir.Dispatcher.register(:being_entered)

    character =
      ManaElixir.EntitySupervisor.create_entity(
        ManaElixir.Character,
        %ManaElixir.Db.Character{},
        pid
      )

    assert_receive {:being_entered, _}
  end
end
