defmodule ManaElixir.DbBackedAccountManagerTest do
  use ExUnit.Case
  doctest ManaElixir.DbBackedAccountManager
  import ManaElixir.DbBackedAccountManager
  import Mox
  alias ManaElixir.Db.Character
  alias ManaElixir.Db.Repo

  setup :verify_on_exit!

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Repo)
  end

  defp sha256(data) do
    :crypto.hash(:sha256, data) |> Base.encode16(case: :lower)
  end

  defp derive_login_hash(register_hash, salt) do
    sha256(sha256(register_hash) <> salt)
  end

  @username "username"
  @register_password_hash "register hash from the client"
  @mail "xxx@xxx.xx"
  @salt <<1, 2, 3, 4>>

  describe "account creation" do
    test "creation of login works" do
      login_hash = derive_login_hash(@register_password_hash, @salt)
      assert check_login(@username, login_hash, @salt) == :bad_login
      {:ok, account} = create_login(@username, @register_password_hash, @mail)
      assert account.username == @username
      assert check_login(@username, login_hash, @salt) == {:ok, account}
    end

    test "attempt to create already existing login should fail" do
      assert {:ok, _} = create_login(@username, @register_password_hash, @mail)
      assert create_login(@username, @register_password_hash, @mail) == :name_taken
    end

    test "bad password does not authenticate" do
      {:ok, _} = create_login(@username, @register_password_hash, @mail)
      assert check_login(@username, "bad login hash", @salt) == :bad_login
    end
  end

  describe "char creation" do
    @name "test"
    @gender 1
    @hair_style 1
    @hair_color 1

    test "creation works" do
      {:ok, account} = create_login(@username, @register_password_hash, @mail)

      {:ok, char} =
        create_char(account, %{
          name: @name,
          gender: @gender,
          hair_style: @hair_style,
          hair_color: @hair_color
        })

      assert char.name == "test"
    end

    test "double creation is prevented" do
      {:ok, account} = create_login(@username, @register_password_hash, @mail)

      {:ok, _} =
        create_char(account, %{
          name: @name,
          gender: @gender,
          hair_style: @hair_style,
          hair_color: @hair_color
        })

      assert create_char(account, %{
               name: @name,
               gender: @gender,
               hair_style: @hair_style,
               hair_color: @hair_color
             }) == :name_taken
    end
  end
end
