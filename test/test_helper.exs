Ecto.Adapters.SQL.Sandbox.mode(ManaElixir.Db.Repo, :manual)

Mox.defmock(ManaElixir.AccountManagerMock, for: ManaElixir.AccountManager)
Mox.defmock(ManaElixir.GameServerMock, for: ManaElixir.GameServer)
Mox.defmock(ManaElixir.GameServerManagerMock, for: ManaElixir.GameServerManager)
Mox.defmock(ManaElixir.TokenbasedDataStorerMock, for: ManaElixir.TokenbasedDataStorer)

ExUnit.start()
