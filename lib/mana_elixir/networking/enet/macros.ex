defmodule ManaElixir.Networking.Enet.Macros do
  defmacro constant(name, value) do
    quote do
      defmacro unquote(name) do
        unquote(value)
      end
    end
  end

  @doc """
  Macro for generating a match of an string in a bitstream

  ## Examples
      iex> ManaElixir.Networking.Enet.Macros.string(msg) = <<5::16, "Hello">>
      ...> msg
      "Hello"
      iex> ManaElixir.Networking.Enet.Macros.string("test")
      <<4::16, "test">>
      iex> ManaElixir.Networking.Enet.Macros.string("hełło")
      <<7::16, "hełło">>
  """
  defmacro string(name, length_field_size \\ 16) do
    if Macro.Env.in_match?(__CALLER__) do
      varname = elem(name, 0) |> Atom.to_string()
      length = :"length_#{varname}" |> Macro.var(nil)

      quote do
        <<unquote(length)::unquote(length_field_size),
          unquote(name)::binary-size(unquote(length))>>
      end
    else
      quote do
        <<byte_size(unquote(name))::unquote(length_field_size), unquote(name)::binary>>
      end
    end
  end
end
