defmodule ManaElixir.Networking.Enet.Debug do
  require Logger
  use Bitwise
  import ManaElixir.Networking.Enet.Protocol

  defp parse_dbg_msg(<<>>) do
    []
  end

  defp parse_dbg_msg(<<dbg_val_type_int8()::8, int8::8, rest::binary>>) do
    [[type: :int8, data: <<int8::8>>] | parse_dbg_msg(rest)]
  end

  defp parse_dbg_msg(<<dbg_val_type_int16()::8, int16::16, rest::binary>>) do
    [[type: :int16, data: <<int16::16>>] | parse_dbg_msg(rest)]
  end

  defp parse_dbg_msg(<<dbg_val_type_int32()::8, int32::32, rest::binary>>) do
    [[type: :int32, data: <<int32::32>>] | parse_dbg_msg(rest)]
  end

  defp parse_dbg_msg(
         <<dbg_val_type_string()::8, dbg_val_type_int16()::8, dbg_length::integer-signed-size(16),
           rest::binary>>
       ) do
    {data, rest} =
      if dbg_length == -1 do
        <<dbg_val_type_int16()::8, actual_length::16, string::binary-size(actual_length),
          rest::binary>> = rest

        {<<actual_length::16, string::binary>>, rest}
      else
        <<string::binary-size(dbg_length), rest::binary>> = rest
        {string, rest}
      end

    [[type: :string, data: data, length: dbg_length] | parse_dbg_msg(rest)]
  end

  def strip_debug_info(<<command_id::16, rest::binary>>) when (command_id &&& dbg_flag()) != 0 do
    stripped_command_id = command_id &&& bnot(dbg_flag())
    parsed_msg = parse_dbg_msg(rest)

    Logger.debug(
      'Received message with dbg info (#{inspect(stripped_command_id)}): #{inspect(parsed_msg)}'
    )

    stripped_msg =
      Enum.reduce(parsed_msg, <<>>, fn msg_part, acc ->
        <<acc::binary, Keyword.fetch!(msg_part, :data)::binary>>
      end)

    <<stripped_command_id::16, stripped_msg::binary>>
  end

  def strip_debug_info(non_dbg_msg) do
    non_dbg_msg
  end
end
