defmodule ManaElixir.Networking.Enet.Protocol do
  import ManaElixir.Networking.Enet.Macros

  # Constants
  constant(errmsg_ok, 0)
  constant(errmsg_failure, 1)
  constant(errmsg_invalid_argument, 5)

  constant(register_exists_username, 0x41)

  constant(create_exists_name, 0x46)

  constant(dbg_flag, 0x8000)

  constant(being_type_character, 4)

  constant(being_action_stand, 0)

  constant(being_direction_down, 0)

  # Value types (from debug messages)
  constant(dbg_val_type_int8, 0)
  constant(dbg_val_type_int16, 1)
  constant(dbg_val_type_int32, 2)
  constant(dbg_val_type_string, 3)
  constant(dbg_val_type_double, 4)

  @message_parts %{
    :map_stat_list_entry => {
      quote(do: {map_id, entity_num, monster_num, player_num, player_ids}),
      quote(
        do: <<map_id::16, entity_num::16, monster_num::16, player_num::16, player_ids::binary>>
      )
    },
    :char_data => {
      quote(
        do:
          {slot, name, gender, hair_style, hair_color, character_points, correction_points,
           num_of_items_equipped, equipment, attribute_count, attributes}
      ),
      quote(
        do:
          <<slot::8, string(name), gender::8, hair_style::8, hair_color::8, character_points::16,
            correction_points::16, num_of_items_equipped::8,
            equipment::bytes-size(num_of_items_equipped), attribute_count::8, attributes::binary>>
      )
    },
    :equipment_item => {
      quote(do: {slot, item_id}),
      quote(do: <<slot::16, item_id::16>>)
    },
    :attribute => {
      quote(do: {attribute_id, base_value_in_256ths, mod_value_in_256ths}),
      quote(do: <<attribute_id::32, base_value_in_256ths::32, mod_value_in_256ths::32>>)
    },
    :attribute_16bit_id => {
      quote(do: {attribute_id, base_value_in_256ths, mod_value_in_256ths}),
      quote(do: <<attribute_id::16, base_value_in_256ths::32, mod_value_in_256ths::32>>)
    },
    :attribute_16bit_id_list => {
      quote(do: {attribute, rest}),
      quote(do: <<attribute::80, rest::binary>>)
    },
    :server_access_info => {
      quote(
        do: {token, game_server_address, game_server_port, chat_server_address, chat_server_port}
      ),
      quote(
        do:
          <<token::binary-size(32), string(game_server_address), game_server_port::16,
            string(chat_server_address), chat_server_port::16>>
      )
    },
    :serialized_char_data => {
      quote(
        do:
          {account_level, gender, hair_style, hair_color, attribute_points, correction_points,
           map_id, x, y}
      ),
      quote(
        do: <<
          account_level::8,
          gender::8,
          hair_style::8,
          hair_color::8,
          attribute_points::16,
          correction_points::16,
          # attribute count
          0::16,
          # status effects
          0::16,
          map_id::16,
          x::16,
          y::16,
          # kill count
          0::16,
          # abilities
          0::16,
          # quest log
          0::16
        >>
      )
    },
    :character_join_message => {
      quote(do: {name, looks_change}),
      quote(do: <<string(name), looks_change::binary>>)
    },
    :looks_change => {
      quote(do: {hair_style, hair_color, num_sprite_layers_changed, list_of_slot_and_items}),
      quote(
        do:
          <<hair_style::8, hair_color::8, num_sprite_layers_changed::8,
            list_of_slot_and_items::binary>>
      )
    }
  }

  @sync_messages %{
    :sync_character_points => {
      quote(do: {char_id, attribute_points, correction_points}),
      quote(do: <<0x01::8, char_id::32, attribute_points::32, correction_points::32>>)
    },
    :sync_character_attribute => {
      quote(do: {char_id, attr_id, base, mod}),
      quote(do: <<0x02::8, char_id::32, attr_id::32, string(base, 8), string(mod, 8)>>)
    },
    :sync_online_status => {
      quote(do: {char_id, online_status}),
      quote(do: <<0x04::8, char_id::32, online_status::8>>)
    }
  }

  @messages %{
    :pamsg_register => {
      quote(do: {version, username, password_hash, email, captcha_response}),
      quote(
        do:
          <<0x0000::16, version::32, string(username), string(password_hash), string(email),
            string(captcha_response)>>
      )
    },
    :apmsg_register_response => {
      quote(do: {error, updatehost, client_data_uri, character_slots}),
      quote(
        do:
          <<0x0002::16, error::8, string(updatehost), string(client_data_uri),
            character_slots::8>>
      )
    },
    :pamsg_request_register_info => {
      quote(do: {}),
      quote(do: <<0x0005::16>>)
    },
    :apmsg_register_info_response => {
      quote(
        do:
          {registration_allowed, min_name_length, max_name_length, captcha_url,
           captcha_url_instructions}
      ),
      quote(
        do:
          <<0x0006::16, registration_allowed::8, min_name_length::8, max_name_length::8,
            string(captcha_url), string(captcha_url_instructions)>>
      )
    },
    :pamsg_login => {
      quote(do: {version, username, password}),
      quote(do: <<0x0010::16, version::32, string(username), string(password)>>)
    },
    :apmsg_login_response => {
      quote(do: {error, update_host, client_data_url, character_slots, characters}),
      quote(
        do:
          <<0x0012::16, error::8, string(update_host), string(client_data_url),
            character_slots::8, characters::binary>>
      )
    },
    :pamsg_logout => {
      quote(do: {}),
      quote(do: <<0x0013::16>>)
    },
    :apmsg_logout_response => {
      quote(do: {error}),
      quote(do: <<0x0014::16, error::16>>)
    },
    :pamsg_login_rndtrgr => {
      quote(do: {username}),
      quote(do: <<0x0015::16, string(username)>>)
    },
    :apmsg_login_rndtrgr_response => {
      quote(do: {random_seed}),
      quote(do: <<0x0016::16, string(random_seed)>>)
    },
    :pamsg_char_create => {
      quote(do: {name, hair_style, hair_color, gender, slot, stat_values}),
      quote(
        do:
          <<0x0020::16, string(name), hair_style::8, hair_color::8, gender::8, slot::8,
            stat_values::binary>>
      )
    },
    :apmsg_char_create_response => {
      quote(do: {error, char_data}),
      quote(do: <<0x0021::16, error::8, char_data::binary>>)
    },
    :pamsg_char_select => {
      quote(do: {slot}),
      quote(do: <<0x0026::16, slot::8>>)
    },
    :apmsg_char_select_response => {
      quote(do: {error, server_access_info}),
      quote(do: <<0x0027::16, error::8, server_access_info::binary>>)
    },
    :pgmsg_connect => {
      quote(do: {token}),
      quote(do: <<0x0050::16, token::binary-size(32)>>)
    },
    :gpmsg_connect_response => {
      quote(do: {error}),
      quote(do: <<0x0051::16, error::8>>)
    },
    :pcmsg_connect => {
      quote(do: {token}),
      quote(do: <<0x0053::16, token::binary-size(32)>>)
    },
    :cpmsg_connect_response => {
      quote(do: {error}),
      quote(do: <<0x0054::16, error::8>>)
    },
    :gpmsg_player_map_change => {
      quote(do: {filename, x, y}),
      quote(do: <<0x0100::16, string(filename), x::16, y::16>>)
    },
    :gpmsg_player_attribute_change => {
      quote(do: {attribute_16bit_id_list}),
      quote(do: <<0x0130::16, attribute_16bit_id_list::binary>>)
    },
    :gpmsg_being_enter => {
      quote(do: {type, being_id, action, x, y, direction, gender, type_specific_enter_data}),
      quote(
        do:
          <<0x0200::16, type::8, being_id::16, action::8, x::16, y::16, direction::8, gender::8,
            type_specific_enter_data::binary>>
      )
    },
    :gamsg_register => {
      quote(do: {name, address, port, password, item_db_version}),
      quote(
        do:
          <<0x0500::16, string(name), string(address), port::16, string(password),
            item_db_version::32>>
      )
    },
    :agmsg_register_response => {
      quote(do: {item_version, password_response, global_var_list}),
      quote(do: <<0x0501::16, item_version::16, password_response::16, global_var_list::binary>>)
    },
    :agmsg_active_map => {
      quote(do: {map_id}),
      quote(do: <<0x0502::16, map_id::16, 0::16, 0::16>>)
    },
    :agmsg_player_enter => {
      quote(do: {token, id, name, char_data}),
      quote(do: <<0x0510::16, token::binary-size(32), id::32, string(name), char_data::binary>>)
    },
    :gamsg_player_data => {
      quote(do: {char_id, player_data}),
      quote(do: <<0x0520::16, char_id::32, player_data::binary>>)
    },
    :gamsg_player_sync => {
      quote(do: {sync_messages}),
      quote(do: <<0x0533::16, sync_messages::binary>>)
    },
    :gamsg_set_var_chr => {
      quote(do: {char_id, var_name, value}),
      quote(do: <<0x0540::16, char_id::32, string(var_name), string(value)>>)
    },
    :gamsg_get_var_chr => {
      quote(do: {char_id, var_name}),
      quote(do: <<0x0541::16, char_id::32, string(var_name)>>)
    },
    :agmsg_get_var_chr_response => {
      quote(do: {char_id, var_name, value}),
      quote(do: <<0x0542::16, char_id::32, string(var_name), string(value)>>)
    },
    :gamsg_statistics => {
      quote(do: {map_stat_list}),
      quote(do: <<0x0560::16, map_stat_list::binary>>)
    },
    :gamsg_transaction => {
      quote(do: {char_id, action, message}),
      quote(do: <<0x0600::16, char_id::32, action::32, string(message)>>)
    }
  }

  @parsable_by_content [{:parse, @messages}]
  @list_parsable_by_content [{:parse_sync_message, @sync_messages}]
  @parsable_by_name [@message_parts]
  @writable [@messages, @sync_messages, @message_parts]

  for {function_name, spec} <- @parsable_by_content do
    for {name, {values, pattern}} <- spec do
      def unquote(function_name)(unquote(pattern)) do
        {unquote(name), unquote(values)}
      end
    end
  end

  for {function_name, spec} <- @list_parsable_by_content do
    for {name, {values, pattern}} <- spec do
      def unquote(function_name)(<<>>) do
        []
      end

      def unquote(function_name)(<<unquote(pattern), rest::binary>>) do
        [{unquote(name), unquote(values)} | unquote(function_name)(rest)]
      end
    end
  end

  for spec <- @parsable_by_name do
    for {name, {values, pattern}} <- spec do
      parse_name = :"#{Atom.to_string(name)}_parse"

      def unquote(parse_name)(unquote(pattern)) do
        unquote(values)
      end
    end
  end

  for spec <- @writable do
    for {name, {values, pattern}} <- spec do
      def unquote(name)(unquote(values)) do
        unquote(pattern)
      end
    end
  end
end
