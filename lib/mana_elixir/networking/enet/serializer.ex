defmodule ManaElixir.Networking.Enet.Serializer do
  import ManaElixir.Networking.Enet.Protocol

  def serialize_char(char = %ManaElixir.Db.Character{}) do
    char_data({
      0,
      char.name,
      char.gender,
      char.hair_style,
      char.hair_color,
      0,
      0,
      0,
      <<>>,
      0,
      <<>>
    })
  end
end
