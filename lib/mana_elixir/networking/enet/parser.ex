defmodule ManaElixir.Networking.Enet.Parser do
  @doc """
  Parses a list of 16bit numbers to an array

  ## Examples
      iex> ManaElixir.Networking.Enet.Parser.parse_list_int16(<<1::16, 2::16>>)
      [1, 2]
      iex> ManaElixir.Networking.Enet.Parser.parse_list_int16(<<>>)
      []
  """
  def parse_list_int16(<<>>) do
    []
  end

  def parse_list_int16(<<val::16, rest::binary>>) do
    [val | parse_list_int16(rest)]
  end
end
