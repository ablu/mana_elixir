defmodule ManaElixir.Networking.Enet.Server.GameServer do
  use GenServer
  require Logger

  defmodule State do
    defstruct [
      :game_server_manager,
      :pending_clients
    ]
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %State{
      game_server_manager: Keyword.get(opts, :game_server_manager),
      pending_clients: %{}
    })
  end

  defp start_client_handler(opts) do
    Logger.debug("New game server client")

    ManaElixir.Networking.Enet.Server.GameServerClient.start_link(opts)
  end

  def init(state) do
    :enet.start_host(
      9604,
      &start_client_handler(channel: &1.channels[0]),
      peer_limit: 255
    )

    [1] = state.game_server_manager.register_server("server name", self())

    {:ok, state}
  end

  def handle_cast(
        {:insert_character, char, token},
        state = %State{pending_clients: pending_clients}
      ) do
    Logger.debug("Registered char at game server: #{inspect(char)}")
    Process.send_after(self(), {:timeout_token, token}, 10_000)
    {:noreply, %State{state | pending_clients: Map.put_new(pending_clients, token, char)}}
  end
end
