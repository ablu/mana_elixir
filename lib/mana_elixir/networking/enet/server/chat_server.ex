defmodule ManaElixir.Networking.Enet.Server.ChatServer do
  use GenServer
  require Logger

  def start_link(port, opts \\ []) do
    GenServer.start_link(__MODULE__, port, opts)
  end

  def start_client_handler(channel) do
    Logger.debug("New chat client")

    ManaElixir.Networking.Enet.Server.ChatClient.start_link(channel)
  end

  def init(port) do
    Logger.debug("Starting chat server")

    :enet.start_host(
      port,
      &start_client_handler(&1.channels[0]),
      peer_limit: 255
    )

    {:ok, {}}
  end
end
