defmodule ManaElixir.Networking.Enet.Server.GameServerClient do
  use GenServer
  require Logger
  import ManaElixir.Networking.Enet.Debug
  import ManaElixir.Networking.Enet.Protocol
  alias ManaElixir.Networking.Enet.Sender

  defmodule State do
    defstruct [:channel, :char]
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %State{channel: Keyword.get(opts, :channel)})
  end

  def init(state) do
    {:ok, state}
  end

  def handle_info({:enet, _channel, {:reliable, msg}}, state) do
    Logger.debug("Received message: #{inspect(msg)}")
    msg = strip_debug_info(msg)
    parsed_msg = parse(msg)
    Logger.debug("Parsed message: #{inspect(parsed_msg)}")
    handle_msg(parsed_msg, state)
  end

  defp send_attributes(char, channel) do
    attributes = ManaElixir.Character.attributes(char)

    serialized_attributes =
      attributes
      |> Enum.map_join(<<>>, fn {id, {base, mod}} ->
        attribute_16bit_id({id, trunc(base * 256), trunc(mod * 256)})
        |> IO.inspect(label: "attribute 15 bit:")
      end)

    Sender.send(channel, gpmsg_player_attribute_change({serialized_attributes}))
  end

  defp handle_msg({:pgmsg_connect, {token}}, state) do
    data_storer = Application.get_env(:mana_elixir, :token_based_storer)
    char = data_storer.get_data(token)

    char_pid = ManaElixir.EntitySupervisor.create_entity(ManaElixir.Character, char)

    Sender.send(state.channel, gpmsg_connect_response({errmsg_ok()}))
    Sender.send(state.channel, gpmsg_player_map_change({"goldenfields/main.tmx", char.x, char.y}))
    looks_update = looks_change({char.hair_style, char.hair_color, 0, <<>>})
    char_data = character_join_message({char.name, looks_update})

    send_attributes(char_pid, state.channel)

    Sender.send(
      state.channel,
      gpmsg_being_enter(
        {being_type_character(), 1, being_action_stand(), char.x, char.y, being_direction_down(),
         char.gender, char_data}
      )
    )

    {:noreply, %State{state | char: char}}
  end
end
