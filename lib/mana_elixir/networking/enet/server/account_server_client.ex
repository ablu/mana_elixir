defmodule ManaElixir.Networking.Enet.Server.AccountServerClient do
  use GenServer
  import ManaElixir.Networking.Enet.Debug
  import ManaElixir.Networking.Enet.Protocol
  import ManaElixir.Networking.Enet.Serializer
  require Logger
  alias ManaElixir.Networking.Enet.Sender

  defmodule State do
    defstruct [
      :channel,
      :client_data_uri,
      :max_num_of_chars,
      :account,
      :salt,
      :game_server_address,
      :game_server_port,
      :chat_server_address,
      :chat_server_port
    ]
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %State{
      channel: Keyword.fetch!(opts, :channel),
      client_data_uri: Keyword.fetch!(opts, :client_data_uri),
      max_num_of_chars: Keyword.fetch!(opts, :max_num_of_chars),
      game_server_address: Keyword.fetch!(opts, :game_server_address),
      game_server_port: Keyword.fetch!(opts, :game_server_port),
      chat_server_address: Keyword.fetch!(opts, :chat_server_address),
      chat_server_port: Keyword.fetch!(opts, :chat_server_port)
    })
  end

  def init(state) do
    {:ok, state}
  end

  def handle_info({:enet, :disconnected, :remote, peer, _id}, state) do
    Logger.debug("Client disconnected: #{inspect(peer)}")

    {:noreply, state}
  end

  def handle_info({:enet, _channel, {:reliable, msg}}, state) do
    Logger.debug("Received message: #{inspect(msg)}")
    msg = strip_debug_info(msg)
    parsed_msg = parse(msg)
    Logger.debug("Parsed message: #{inspect(parsed_msg)}")
    handle_msg(parsed_msg, state)
  end

  defp handle_msg({:pamsg_request_register_info, {}}, state = %State{}) do
    Logger.debug("Received request for register info")

    msg = apmsg_register_info_response({1, 1, 255, "", ""})
    Sender.send(state.channel, msg)

    {:noreply, state}
  end

  defp handle_msg(
         {:pamsg_login, {version, username, password}},
         %State{client_data_uri: client_data_uri, max_num_of_chars: max_num_of_chars, salt: salt} =
           state
       )
       when salt != nil do
    Logger.debug("Login from user #{inspect(username)} with version #{inspect(version)}")

    account_manager = Application.get_env(:mana_elixir, :account_manager)

    {error_code, account} =
      case account_manager.check_login(username, password, salt) do
        {:ok, account} ->
          {errmsg_ok(), account}

        :bad_login ->
          {errmsg_invalid_argument(), nil}
      end

    char_data =
      if account == nil do
        <<>>
      else
        account.characters |> Enum.reduce(<<>>, fn char, acc -> acc <> serialize_char(char) end)
      end

    msg = apmsg_login_response({error_code, "", client_data_uri, max_num_of_chars, char_data})

    Sender.send(state.channel, msg)

    {:noreply, %State{state | account: account, salt: nil}}
  end

  defp handle_msg({:pamsg_logout, {}}, state) do
    msg = apmsg_logout_response({errmsg_ok()})
    Sender.send(state.channel, msg)

    {:noreply, %State{state | account: nil, salt: nil}}
  end

  defp handle_msg({:pamsg_login_rndtrgr, {_username}}, state) do
    salt = :crypto.strong_rand_bytes(4) |> IO.inspect(label: "rand bytes")
    msg = apmsg_login_rndtrgr_response({salt})
    Sender.send(state.channel, msg)
    {:noreply, %State{state | salt: salt}}
  end

  defp handle_msg(
         {:pamsg_register, {_version, username, password_hash, email, _captcha_response}},
         %State{client_data_uri: client_data_uri, max_num_of_chars: max_num_of_chars} = state
       ) do
    account_manager = Application.get_env(:mana_elixir, :account_manager)

    {error_code, account} =
      case account_manager.create_login(username, password_hash, email) do
        {:ok, account} ->
          {errmsg_ok(), account}

        :name_taken ->
          {register_exists_username(), nil}
      end

    msg = apmsg_register_response({error_code, "", client_data_uri, max_num_of_chars})
    Sender.send(state.channel, msg)

    {:noreply, %State{state | account: account}}
  end

  defp handle_msg(
         {:pamsg_char_create, {name, hair_style, hair_color, gender, _slot, _binary_stat_values}},
         %State{account: account} = state
       )
       when account != nil do
    account_manager = Application.get_env(:mana_elixir, :account_manager)

    {msg, state} =
      case account_manager.create_char(account, %{
             name: name,
             hair_style: hair_style,
             hair_color: hair_color,
             gender: gender
           }) do
        {:ok, char} ->
          Logger.debug("Created char: #{inspect(char)}")

          {apmsg_char_create_response({errmsg_ok(), serialize_char(char)}),
           update_in(state.account.characters, &(&1 ++ [char]))}

        :name_taken ->
          Logger.debug("name taken")
          {apmsg_char_create_response({create_exists_name(), <<>>}), state}
      end

    Sender.send(state.channel, msg)

    {:noreply, state}
  end

  defp handle_msg({:pamsg_char_select, {slot}}, state = %State{account: account})
       when account != nil do
    msg =
      case account.characters |> Enum.at(slot) do
        nil ->
          Logger.warn("Attempting to log in with invalid character slot")
          apmsg_char_select_response({errmsg_failure(), <<>>})

        char ->
          data_storer = Application.get_env(:mana_elixir, :token_based_storer)
          token = data_storer.register_data(char)

          access_info =
            server_access_info({
              token,
              state.game_server_address,
              state.game_server_port,
              state.chat_server_address,
              state.chat_server_port
            })

          apmsg_char_select_response({
            errmsg_ok(),
            access_info
          })
      end

    Sender.send(state.channel, msg)

    {:noreply, %State{state | account: nil, salt: nil}}
  end
end
