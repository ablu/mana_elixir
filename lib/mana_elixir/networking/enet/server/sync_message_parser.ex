defmodule ManaElixir.Networking.Enet.Server.SyncMessageParser do
  import ManaElixir.Networking.Enet.Protocol

  defp parse_float(s) do
    {val, ""} = Float.parse(s)
    val
  end

  defp parse_bool(1) do
    true
  end

  defp parse_bool(_) do
    false
  end

  defp parse_msg({:sync_online_status, {char_id, online}}) do
    {:sync_online_status, char_id, parse_bool(online)}
  end

  defp parse_msg({:sync_character_attribute, {char_id, attr_id, base, mod}}) do
    {:sync_character_attribute, char_id, attr_id, parse_float(base), parse_float(mod)}
  end

  defp parse_msg(other) do
    other
  end

  def parse(syncmessage) do
    parse_sync_message(syncmessage) |> Enum.map(fn msg -> parse_msg(msg) end)
  end
end
