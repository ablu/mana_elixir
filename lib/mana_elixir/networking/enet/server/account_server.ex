defmodule ManaElixir.Networking.Enet.Server.AccountServer do
  use GenServer
  require Logger

  defmodule State do
    defstruct [
      :client_data_uri,
      :max_num_of_chars,
      :game_server_address,
      :game_server_port,
      :chat_server_address,
      :chat_server_port
    ]
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %State{
      client_data_uri: Keyword.fetch!(opts, :client_data_uri),
      max_num_of_chars: Keyword.fetch!(opts, :max_num_of_chars),
      game_server_address: Keyword.fetch!(opts, :game_server_address),
      game_server_port: Keyword.fetch!(opts, :game_server_port),
      chat_server_address: Keyword.fetch!(opts, :chat_server_address),
      chat_server_port: Keyword.fetch!(opts, :chat_server_port)
    })
  end

  def start_client_handler(opts) do
    Logger.debug("New client: #{inspect(opts)}")

    ManaElixir.Networking.Enet.Server.AccountServerClient.start_link(opts)
  end

  def init(state) do
    Logger.info("starting client server")

    :enet.start_host(
      9601,
      &start_client_handler(
        channel: &1.channels[0],
        client_data_uri: state.client_data_uri,
        max_num_of_chars: state.max_num_of_chars,
        game_server_address: state.game_server_address,
        game_server_port: state.game_server_port,
        chat_server_address: state.chat_server_address,
        chat_server_port: state.chat_server_port
      ),
      peer_limit: 255
    )

    {:ok, state}
  end
end
