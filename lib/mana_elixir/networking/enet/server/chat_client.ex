defmodule ManaElixir.Networking.Enet.Server.ChatClient do
  use GenServer
  import ManaElixir.Networking.Enet.Debug
  import ManaElixir.Networking.Enet.Protocol
  require Logger
  alias ManaElixir.Networking.Enet.Sender

  defmodule State do
    defstruct [
      :channel
    ]
  end

  def start_link(channel, opts \\ []) do
    GenServer.start_link(__MODULE__, %State{channel: channel}, opts)
  end

  def init(state) do
    {:ok, state}
  end

  def handle_info({:enet, _channel, {:reliable, msg}}, state) do
    Logger.debug("Received message: #{inspect(msg)}")
    msg = strip_debug_info(msg)
    parsed_msg = parse(msg)
    handle_msg(parsed_msg, state)
  end

  defp handle_msg({:pcmsg_connect, {token}}, state) do
    Logger.debug("New client on chat server with token #{inspect(token)}")
    msg = cpmsg_connect_response({errmsg_ok()})
    Sender.send(state.channel, msg)
    {:noreply, state}
  end
end
