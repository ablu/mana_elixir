defprotocol ManaElixir.Networking.Enet.Sender do
  def send(send_target, data)
end

defimpl ManaElixir.Networking.Enet.Sender, for: PID do
  require Logger

  def send(pid, data) do
    Logger.debug("Sending: #{inspect(data)}. In hex: #{inspect(Base.encode16(data))}")
    :enet.send_reliable(pid, data)
  end
end

defimpl ManaElixir.Networking.Enet.Sender, for: Function do
  def send(function, data) do
    function.(data)
  end
end
