defmodule ManaElixir.Config do
  def get!(var) do
    env_variable = String.upcase(to_string(var))

    case System.get_env(env_variable) || Application.get_env(:mana_elixir, var) do
      nil -> raise "Missing environment variable #{inspect(env_variable)}!"
      result -> result
    end
  end
end
