defmodule ManaElixir.TokenbasedDataStorer do
  @callback register_data(term) :: binary()
  @callback get_data(String.t()) :: term | nil
end

defmodule ManaElixir.InMemoryTokenbasedDataStorer do
  @behaviour ManaElixir.TokenbasedDataStorer
  use GenServer

  @token_length 32

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %{}, opts)
  end

  @impl true
  def register_data(data, timeout \\ 10_000, server \\ __MODULE__) do
    GenServer.call(server, {:register, data, timeout})
  end

  @impl true
  def get_data(token, server \\ __MODULE__) do
    GenServer.call(server, {:get, token})
  end

  @impl true
  def init(args) do
    {:ok, args}
  end

  @impl true
  def handle_call({:register, data, timeout}, _from, state) do
    token = :crypto.strong_rand_bytes(@token_length)
    Process.send_after(self(), {:timeout, token}, timeout)
    {:reply, token, Map.put(state, token, data)}
  end

  @impl true
  def handle_call({:get, token}, _from, state) do
    {:reply, Map.get(state, token), state}
  end

  @impl true
  def handle_info({:timeout, token}, state) do
    {:noreply, Map.delete(state, token)}
  end
end
