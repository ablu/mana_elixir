defmodule ManaElixir.Being do
  defstruct [
    :position,
    :attributes
  ]
end
