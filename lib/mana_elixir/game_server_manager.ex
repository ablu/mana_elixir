defmodule ManaElixir.GameServerManager do
  @callback register_server(String.t(), pid()) :: [integer()]
  @callback unregister_server(String.t()) :: :ok
  @callback get_server_for_map(integer()) :: pid()
end

defmodule ManaElixir.GameServerManagerImpl do
  @behaviour ManaElixir.GameServerManager
  use GenServer
  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %{}, opts)
  end

  @impl true
  def register_server(name, pid, server \\ __MODULE__) do
    GenServer.call(server, {:register_server, name, pid})
  end

  @impl true
  def unregister_server(name, server \\ __MODULE__) do
    GenServer.cast(server, {:unregister_server, name})
  end

  @impl true
  def get_server_for_map(map_id, server \\ __MODULE__) do
    GenServer.call(server, {:get_server_for_map, map_id})
  end

  @impl true
  def init(%{}) do
    {:ok, %{}}
  end

  @impl true
  def handle_call({:register_server, name, pid}, _from, %{}) do
    maps_to_activate = [1]

    {:reply, maps_to_activate, %{1 => {name, pid}}}
  end

  @impl true
  def handle_call({:get_server_for_map, _map_id}, _from, state = %{1 => {_name, pid}}) do
    {:reply, pid, state}
  end

  @impl true
  def handle_cast({:unregister_server, name}, %{1 => {name, _}}) do
    {:noreply, %{}}
  end
end
