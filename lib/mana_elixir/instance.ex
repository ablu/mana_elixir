defmodule ManaElixir.Instance do
  use GenServer

  def start_link(args \\ []) do
    GenServer.start_link(__MODULE__, [], args)
  end

  def init([]) do
    pid = Registry.start_link(keys: :duplicate, name: __MODULE__)
    {:ok, pid}
  end

  def insert_being(instance, data) do
    Registry.register(instance, :beings, data)
    ManaElixir.Dispatcher.notify(:being_entered, data)
  end

  def get_all_entities(instance) do
    Registry.lookup(instance, :beings)
  end
end
