defmodule ManaElixir.EntitySupervisor do
  use DynamicSupervisor

  def start_link(args) do
    DynamicSupervisor.start_link(__MODULE__, [], args)
  end

  def create_entity(type, args, server \\ __MODULE__) do
    {:ok, pid} = DynamicSupervisor.start_child(server, {type, args})
    pid
  end

  @impl true
  def init([]) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
