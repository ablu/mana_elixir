defmodule ManaElixir.Character do
  use GenServer

  def start_link(char) do
    GenServer.start_link(__MODULE__, char)
  end

  def attributes(char) do
    GenServer.call(char, :attributes)
  end

  def init(char) do
    data = %ManaElixir.Being{
      position: {char.x, char.y},
      attributes: %{
        # Strength
        1 => {1.0, 1.0},
        # Agility
        2 => {1.0, 1.0},
        # Vitality
        3 => {1.0, 1.0},
        # Intelligence
        4 => {1.0, 1.0},
        # Dexterity
        5 => {1.0, 1.0},
        # Willpower
        6 => {1.0, 1.0},
        # Movement
        16 => {1.0, 1.0}
      }
    }

    ManaElixir.Instance.insert_being(ManaElixir.Instance, data)

    {:ok, data}
  end

  def handle_call(:attributes, _from, data) do
    {:reply, data.attributes, data}
  end
end
