defmodule ManaElixir.GameServer do
  @callback insert_character(pid(), String.t(), map()) :: :ok
end

defmodule ManaElixir.GameServerImpl do
  @behaviour ManaElixir.GameServer

  @impl true
  def insert_character(server, token, character) do
    GenServer.cast(server, {:insert_character, token, character})
  end
end
