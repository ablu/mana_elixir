defmodule ManaElixir.Db.Account do
  use Ecto.Schema
  import Ecto.Changeset
  alias ManaElixir.Db.Character

  schema "accounts" do
    field(:username, :string)
    field(:password_hash, :string)
    field(:email_hash, :string)

    has_many(:characters, Character)
  end

  def changeset(account, params \\ %{}) do
    account
    |> cast(params, [:username, :password_hash, :email_hash])
    |> cast_assoc(:characters)
    |> validate_required([:username, :password_hash, :email_hash])
    |> unique_constraint(:username)
  end
end
