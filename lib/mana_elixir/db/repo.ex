defmodule ManaElixir.Db.Repo do
  use Ecto.Repo,
    otp_app: :mana_elixir,
    adapter: Ecto.Adapters.Postgres
end
