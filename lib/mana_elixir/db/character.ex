defmodule ManaElixir.Db.Character do
  use Ecto.Schema
  import Ecto.Changeset
  alias ManaElixir.Db.Account

  schema "characters" do
    field(:name, :string)
    field(:hair_style, :integer)
    field(:hair_color, :integer)
    field(:gender, :integer)
    field(:x, :integer)
    field(:y, :integer)

    belongs_to(:account, Account)
    timestamps()
  end

  def changeset(char, params \\ %{}) do
    char
    |> cast(params, [:name, :hair_style, :hair_color, :gender, :x, :y])
    |> validate_required([:name, :hair_style, :hair_color, :gender, :x, :y])
    |> unique_constraint(:name)
  end
end
