defmodule ManaElixir.AccountManager do
  @callback check_login(String.t(), binary(), binary()) :: {:ok, map()} | :bad_login

  @callback create_login(String.t(), binary(), String.t()) :: {:ok, map()} | :name_taken

  @callback create_char(map(), term) :: {:ok, map()} | :name_taken
end

defmodule ManaElixir.DbBackedAccountManager do
  @behaviour ManaElixir.AccountManager

  require Logger
  alias ManaElixir.Db.Account
  alias ManaElixir.Db.Character
  alias ManaElixir.Db.Repo

  defp format_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end

  defp sha256(data) do
    :crypto.hash(:sha256, data) |> Base.encode16(case: :lower)
  end

  @impl true
  def check_login(username, user_password_hash, network_salt) do
    case Account |> Repo.get_by(username: username) |> Repo.preload([:characters]) do
      nil ->
        Logger.debug("Login attempt for non-existing user #{inspect(username)}")
        :bad_login

      account ->
        if sha256(account.password_hash <> network_salt) == user_password_hash do
          {:ok, account}
        else
          Logger.debug("Login failed for user #{inspect(username)}")
          :bad_login
        end
    end
  end

  @impl true
  def create_login(username, user_password_hash, email) do
    password_hash = user_password_hash |> sha256()

    email_hash = Comeonin.Argon2.hashpwsalt(email)

    case Repo.insert(
           Account.changeset(%Account{}, %{
             username: username,
             password_hash: password_hash,
             email_hash: email_hash,
             characters: []
           })
         ) do
      {:error, changeset} ->
        case format_errors(changeset) do
          %{username: ["has already been taken"]} -> :name_taken
        end

      result ->
        result
    end
  end

  @impl true
  def create_char(account = %Account{}, character) do
    character = Map.merge(character, %{x: 2272, y: 1472})

    case Ecto.build_assoc(account, :characters)
         |> Character.changeset(character)
         |> Repo.insert() do
      {:ok, char} ->
        {:ok, char}

      {:error, changeset} ->
        case format_errors(changeset) do
          %{name: ["has already been taken"]} -> :name_taken
        end
    end
  end
end
