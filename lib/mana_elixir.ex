defmodule ManaElixir do
  use Application
  alias ManaElixir.Config

  def start(_type, _args) do
    Supervisor.start_link(
      [
        {ManaElixir.Db.Repo, [url: Config.get!(:db_url)]},
        {ManaElixir.InMemoryTokenbasedDataStorer, name: ManaElixir.InMemoryTokenbasedDataStorer},
        {ManaElixir.GameServerManagerImpl, name: ManaElixir.GameServerManagerImpl},
        {ManaElixir.Networking.Enet.Server.GameServer,
         [
           game_server_manager: ManaElixir.GameServerManagerImpl
         ]},
        {ManaElixir.Networking.Enet.Server.AccountServer,
         [
           client_data_uri: Config.get!(:client_data_uri),
           max_num_of_chars: Config.get!(:max_num_of_chars),
           game_server_address: Config.get!(:game_server_address),
           game_server_port: Config.get!(:game_server_port),
           chat_server_address: Config.get!(:chat_server_address),
           chat_server_port: Config.get!(:chat_server_port)
         ]},
        {ManaElixir.Networking.Enet.Server.ChatServer, Config.get!(:chat_server_port)},
        ManaElixir.Dispatcher,
        ManaElixir.Instance,
        {ManaElixir.EntitySupervisor, name: ManaElixir.EntitySupervisor}
      ],
      strategy: :one_for_one,
      name: __MODULE__
    )
  end
end
