defmodule ManaElixir.Db.Repo.Migrations.CharacterCoordinates do
  use Ecto.Migration

  def change do
    alter table(:characters) do
      add(:x, :integer, null: false)
      add(:y, :integer, null: false)
    end
  end
end
