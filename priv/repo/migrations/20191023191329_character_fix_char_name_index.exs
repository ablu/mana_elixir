defmodule ManaElixir.Db.Repo.Migrations.CharacterFixCharNameIndex do
  use Ecto.Migration

  def change do
    create(unique_index(:characters, :name))
    drop(unique_index(:characters, :char_name))
  end
end
