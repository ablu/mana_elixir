defmodule ManaElixir.Db.Repo.Migrations.CreateCharacters do
  use Ecto.Migration

  def change do
    create table(:characters) do
      add(:account_id, references(:accounts, on_delete: :delete_all), null: false)
      add(:char_name, :string, null: false)
      add(:hair_style, :integer, null: false)
      add(:hair_color, :integer, null: false)
      add(:gender, :integer, null: false)

      timestamps()
    end

    create(unique_index(:characters, :char_name))
  end
end
