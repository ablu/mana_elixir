defmodule ManaElixir.Db.Repo.Migrations.CharacterCharNameRename do
  use Ecto.Migration

  def change do
    rename(table(:characters), :char_name, to: :name)
  end
end
